from django.apps import AppConfig


class CampaignsAppConfig(AppConfig):
    name = 'campaigns'
    verbose_name = 'Campaigns'

    def ready(self):
        from . import signals  # noqa F401
        from . import checks  # noqa F401
