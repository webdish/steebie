from django.urls import path, re_path

from .views.api import v1
from .views import webhooks


api_products_patterns = (
    [
        path('', view=v1.ProductsResource.as_view(), name='list'),
        path('<int:pk>', view=v1.ProductResource.as_view(), name='product'),
        path('<int:pk>/review', view=v1.ProductReviewResource.as_view(), name='review'),
        path('<int:pk>/discount', view=v1.ProductDiscountResource.as_view(), name='discount'),
        path('<int:pk>/checkout', view=v1.ProductCheckoutResource.as_view(), name='checkout'),
        path('images/upload', view=v1.ProductImage.as_view(), name='image_upload'),
        path('<int:pk>/breakdown', view=v1.ProductPriceBreakdownResource.as_view(), name='breakdown'),
        path('<int:pk>/stats', view=v1.ProductStatsResource.as_view(), name='stats'),
    ],
    'products'
)


api_orders_patterns = (
    [
        path('', view=v1.OrdersResource.as_view(), name='orders'),
        path('confirm', view=v1.OrdersConfirmationResource.as_view(), name='confirmation'),
    ],
    'orders'
)


# Public

public_api_patterns = (
    [
        re_path('products[/]?', view=v1.ProductsListResource.as_view(), name='products_list'),
    ],
    'public'
)
