import logging
import re
from urllib.parse import urlparse

from django.db.transaction import atomic
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

import requests
from fake_useragent import UserAgent
from rest_framework.exceptions import ValidationError
from rest_framework.fields import (
    CharField,
    ListField,
    BooleanField,
    DecimalField,
    IntegerField,
    EmailField,
    ChoiceField,
)
from rest_framework.relations import PrimaryKeyRelatedField, RelatedField
from rest_framework.serializers import ModelSerializer, Serializer

from base.validators import joined_list_length, alphanum_string, gt_zero, percent, alphanum_space_string
from campaigns.models import Product, Image, Status, Platform
from orders.models import Order, OrderStatus

log = logging.getLogger(__name__)


class ProductStatusSerializer(Serializer):
    status = ChoiceField(choices=Status.choices(), required=False)


class ImagesURLField(RelatedField):
    def to_representation(self, value):
        return {'id': value.pk, 'url': value.img.url}


class ProductSerializer(ModelSerializer):
    keywords = ListField(
        source='keywords_list',
        default=[],
        child=CharField(max_length=255, validators=[alphanum_string]),
        required=False,
        max_length=5,
        validators=[joined_list_length]
    )
    tags = ListField(
        source='tags_list',
        default=[],
        child=CharField(max_length=255, validators=[alphanum_space_string]),
        required=False,
        max_length=250
    )
    private = BooleanField(default=False)
    price_after_reward = DecimalField(max_digits=9, decimal_places=2, read_only=True)
    images = PrimaryKeyRelatedField(many=True, queryset=Image.objects.all(), write_only=True)
    images_urls = ImagesURLField(many=True, source='images', read_only=True)
    shopify_url = CharField(source='get_proxy_url', read_only=True)
    reward_amount = CharField(source='get_reward', read_only=True)
    platform = CharField(default='AMAZON', read_only=True)
    promo_code = CharField(source='get_promo_code', read_only=True)
    platform_name = CharField(source='get_platform_display', read_only=True)
    status_name = CharField(source='get_status_display', read_only=True)
    is_editable = BooleanField(read_only=True)
    is_ended = BooleanField(read_only=True)
    is_archived = BooleanField(read_only=True)
    is_approved = BooleanField(read_only=True)
    is_ready_for_review = BooleanField(read_only=True)
    is_ready_for_checkout = BooleanField(read_only=True)

    class Meta:
        model = Product
        exclude = ('user', 'is_fake')
        read_only_fields = (
            'id',
            'shopify_id',
            'review',
            'handle',
            'status',
            'published_at',
            'reward_amount',
            'promo_code',
        )

    def validate_date_start(self, value):
        if value < timezone.now().replace(hour=0, minute=0, second=0, microsecond=0):
            raise ValidationError(_('Must be not earlier than today.'))
        return value

    def validate_date_end(self, value):
        if value <= timezone.now():
            raise ValidationError(_('Must be in the future.'))
        return value

    def validate_price(self, value):
        gt_zero(value)
        return value

    def validate_reward(self, value):
        percent(value)
        return value

    def validate_daily_limit(self, value):
        gt_zero(value)
        return value

    @staticmethod
    def _rm_amazon_query_string(url):
        affiliate_link = urlparse(url)
        # Let's try to split a path by the first occurence of `/param=`;
        # `param` may be any alphanumeric string, e.g. `ref`
        parts = re.split(r'/[\w.\-_]+?=', affiliate_link.path, maxsplit=1)
        if len(parts) > 1:
            path = f'{parts[0]}/'
        else:
            path = affiliate_link.path

        return f'{affiliate_link.scheme}://{affiliate_link.netloc}{path}'

    def validate_affiliate_link(self, value):
        if self.initial_data.get('platform', Platform.AMAZON.name) == Platform.AMAZON.name:
            return self._rm_amazon_query_string(value)
        else:
            return value

    def _validate_link(self, data):
        if data['platform'] == Platform.AMAZON.name:
            self._validate_link_amazon(data)

    def _validate_link_amazon(self, data):
        if self.partial:
            # Partial update
            value = data.get('affiliate_link')
            keywords_list = data.get('keywords_list')

            if not value and keywords_list:
                keywords_url = Product.get_amazon_keywords_url(self.instance.affiliate_link, keywords_list)
                if not self._check_url(keywords_url):
                    raise ValidationError("Provided URL doesn't work with keywords.")
        else:
            # Object creation
            value = data.get('affiliate_link')

            if not re.match(r'http(s)?://(www.)?amazon\.com/.', value):
                raise ValidationError({'affiliate_link': _('Link should belong to amazon.com domain.')})

            keywords_url = Product.get_amazon_keywords_url(value, data.get('keywords_list'))
            if not self._check_url(keywords_url):
                raise ValidationError({'affiliate_link': "Provided URL doesn't work."})

    @staticmethod
    def _check_url(url):
        ua = UserAgent(fallback='Mozilla/5.0 (Windows NT 6.2; Win64; x64; rv:16.0.1) Gecko/20121011 Firefox/16.0.1')
        headers = {
            'User-Agent': ua.random,
        }

        try:
            resp = requests.get(url, timeout=2, headers=headers)
            return resp.status_code == 200
        except Exception as e:
            log.error(e, exc_info=True)
            return

    def _validate_dates(self, date_start, date_end):
        if date_start >= date_end:
            raise ValidationError({'date_end': _("Must be greater than 'date_start'.")})

    def validate(self, data):
        # Set platform
        if 'platform' not in data:
            data['platform'] = Platform.AMAZON.name

        # Dates
        if self.partial:
            if 'date_start' in data or 'date_end' in data:
                self._validate_dates(
                    data.get('date_start', self.instance.date_start),
                    data.get('date_end', self.instance.date_end)
                )
        else:
            self._validate_dates(data['date_start'], data['date_end'])

        # Affiliate link
        self._validate_link(data)

        return data

    @staticmethod
    def _check_images(images):
        for image in images:
            if image.product:
                raise ValidationError({'images': [f'Image {image.pk} already belongs to another product.']})
            yield image

    def create(self, validated_data):
        images = validated_data.pop('images')

        with atomic():
            # Create product
            product = Product.objects.create(**validated_data)

            # Create default variant
            product.variants.create(price=product.price_after_reward, compare_at_price=product.price)

            # Attach images
            for image in self._check_images(images):
                image.product = product
                image.save()

            return product

    def update(self, instance, validated_data):
        price_original = instance.price
        images = validated_data.pop('images', None)
        existing_images_pks = set(instance.images.all().values_list('pk', flat=True))

        with atomic():
            # Update product
            for attr, value in validated_data.items():
                setattr(instance, attr, value)
            instance.save()

            # Update default variant
            if instance.price != price_original:
                variant = instance.default_variant
                variant.price = instance.price_after_reward
                variant.compare_at_price = instance.price
                variant.save()

            # Attach images
            if images:
                for image in self._check_images(images):
                    image.product = instance
                    image.save()

                lost_images = existing_images_pks - set(images)
                if lost_images:
                    Image.objects.filter(pk__in=lost_images).delete()

            return instance


class StripeCheckoutSerializer(Serializer):
    token = CharField(max_length=255)


class OrderStatusSerializer(Serializer):
    status = ChoiceField(choices=OrderStatus.choices(), required=False)


class OrderedProductSerializer(ModelSerializer):
    reward = DecimalField(source='get_reward', max_digits=9, decimal_places=2)
    image = CharField(source='image_url_local')

    class Meta:
        model = Product
        fields = (
            'id',
            'title',
            'reward',
            'image',
        )


class OrderSerializer(ModelSerializer):
    customer_email = EmailField(source='customer.email')
    product = OrderedProductSerializer()

    class Meta:
        model = Order
        exclude = ('customer', 'email_sent')


class OrdersConfirmSerializer(Serializer):
    orders = ListField(
        child=IntegerField(min_value=1),
        allow_empty=False
    )
    approved = BooleanField()
