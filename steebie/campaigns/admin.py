from django.contrib import admin
from django.utils.safestring import mark_safe

from .models import Product, Variant, Image, FakeUpload, PromoCode


class FakeUploadAdmin(admin.ModelAdmin):

    list_display = ['__str__', 'date_started', 'date_ended', 'is_successful']

    def save_model(self, request, obj, *args, **kwargs):
        obj.uploaded_by = request.user
        super().save_model(request, obj, *args, **kwargs)


class ProductAdmin(admin.ModelAdmin):

    readonly_fields = ['id', 'shopify_url']

    def shopify_url(self, obj):
        if obj.get_proxy_url():
            return mark_safe(f'<a href="{obj.get_proxy_url()}" target="_blank">Open</a>')
    shopify_url.short_description = 'Shopify URL'


admin.site.register(FakeUpload, FakeUploadAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register([Variant, Image, PromoCode])
