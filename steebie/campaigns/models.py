import os
from decimal import Decimal
from urllib.parse import urlencode
import math
import logging

from django.contrib.sites.models import Site
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.db import models
from django.db.models import F, QuerySet, Count, Q
from django.db.transaction import atomic
from django.urls import reverse_lazy
from django.conf import settings
from django.utils import timezone
from django.utils.timezone import utc
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _

import pyactiveresource.connection
import shopify

from apis.analytics import Analytics
from base.models.shopify import BaseShopify
from base.validators import promo_limits
from base.validators import percent, gt_zero
from base.validators import keywords
from orders.models import OrderStatus, Order
from utils.enums import ChoiceEnum
from payments.models import STRIPE_FEE_FIXED, STRIPE_FEE_PERCENT, StripeRefund
from taskapp.celery import publish_product, sync_product
from utils.mail import create_html_email
from utils.numbers import round_decimal
from utils.shopify import set_shopify_site

log = logging.getLogger(__name__)


class Review(ChoiceEnum):
    APPROVED = _('Approved')
    DECLINED = _('Declined')


class Status(ChoiceEnum):
    DRAFT = _('Draft')
    REVIEW = _('In Review')
    APPROVED = _('Approved')
    PAID = _('Pending')
    ACTIVE = _('Active')
    ENDED = _('Ended')
    ARCHIVED = _('Archived')

    @classmethod
    def drafts(cls):
        return cls.DRAFT.name, cls.REVIEW.name, cls.APPROVED.name


class Platform(ChoiceEnum):
    AMAZON = _('Amazon.com')


class CustomCollection(BaseShopify):
    shopify_id = models.BigIntegerField(unique=True, db_index=True, null=True, blank=True)
    title = models.CharField(max_length=255)
    handle = models.CharField(max_length=255, unique=True)
    published_at = models.DateTimeField(null=True)
    updated_at = models.DateTimeField(null=True)

    shopify_resource_cls = shopify.CustomCollection

    shopify_fields = [
        'id',
        'title',
        'handle',
        'published_at',
        'updated_at',
    ]

    shopify_fields_map = {
        'id': 'shopify_id',
    }

    def __str__(self):
        return self.title

    PRIVATE_HANDLE = 'unlisted'
    PRIVATE_TITLE = 'Unlisted'

    @classmethod
    def get_private(cls):
        try:
            return cls.objects.get(handle=cls.PRIVATE_HANDLE)
        except cls.DoesNotExist:
            pass

    @classmethod
    def create_private(cls):
        instance = cls.objects.create(handle=cls.PRIVATE_HANDLE, title=cls.PRIVATE_TITLE)
        instance.sync()

        return instance


class Collect(BaseShopify):
    shopify_id = models.BigIntegerField(unique=True, db_index=True, null=True, blank=True)
    collection = models.ForeignKey(
        'CustomCollection',
        to_field='shopify_id',
        on_delete=models.CASCADE,
        related_name='collects'
    )
    product = models.ForeignKey('Product', to_field='shopify_id', on_delete=models.CASCADE, related_name='collects')
    created_at = models.DateTimeField(null=True)
    updated_at = models.DateTimeField(null=True)

    shopify_resource_cls = shopify.CustomCollection

    shopify_fields = [
        'id',
        'collection_id',
        'product_id',
        'created_at',
        'updated_at',
    ]

    shopify_fields_map = {
        'id': 'shopify_id',
        'collection_id': 'collection',
        'product_id': 'product',
    }


class ProductsQuerySet(QuerySet):

    def send_daily_report(self):
        if self.exists():
            user = self.first().user
            email = create_html_email(
                template='emails/campaign/daily_report.html',
                context={
                    'objects': self,
                    'name': user.get_full_name(),
                    'orders_pending_total': Order.objects.filter(product__in=self).pending_approval().count(),
                    'domain': Site.objects.get_current().domain,
                },
                subject='Steebie | Daily Campaign Report',
                to=[user.email]
            )
            email.send(fail_silently=True)


class Product(BaseShopify):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._status = self.status
        self._shopify_tags = self.tags

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.PROTECT, related_name='products', editable=False
    )
    shopify_id = models.BigIntegerField(unique=True, db_index=True, null=True, blank=True)
    reward = models.DecimalField(max_digits=4, decimal_places=2, validators=[percent])
    affiliate_link = models.URLField(max_length=255)
    platform = models.CharField(max_length=255, choices=Platform.choices())
    title = models.CharField(max_length=255)
    body_html = models.TextField(null=True, blank=True)
    vendor = models.CharField(max_length=255, null=True, blank=True)
    product_type = models.CharField(max_length=255, null=True, blank=True)
    date_start = models.DateTimeField()
    date_end = models.DateTimeField()
    daily_limit = models.PositiveIntegerField(validators=[gt_zero])
    private = models.BooleanField(default=False)
    tags = models.TextField(null=True, blank=True)
    keywords = models.CharField(
        max_length=255, validators=[keywords], null=True, blank=True,
        help_text=_('Space-separated list of Amazon keywords. Up to 5')
    )
    handle = models.CharField(max_length=255, null=True, blank=True, editable=False)
    review = models.CharField(max_length=255, choices=Review.choices(), null=True, blank=True)
    status = models.CharField(max_length=255, choices=Status.choices(), default=Status.DRAFT.name)
    promo_code = models.ForeignKey(
        'PromoCode', null=True, blank=True, on_delete=models.PROTECT, related_name='products'
    )
    published_at = models.DateTimeField(null=True, blank=True)
    is_fake = models.BooleanField(default=False)
    price = models.DecimalField(max_digits=9, decimal_places=2)

    shopify_resource_cls = shopify.Product

    objects = ProductsQuerySet.as_manager()

    class Meta:
        ordering = ['-date_start', '-id']

    shopify_nested = [
        'variants',
        'images',
    ]

    shopify_fields = [
        'id',
        'title',
        'body_html',
        'vendor',
        'product_type',
        'tags',
        'handle',
        'published_at',
    ]

    shopify_fields_map = {
        'id': 'shopify_id',
        'tags': 'shopify_tags',
    }

    def __str__(self):
        return self.title

    # URLs

    def get_absolute_url(self):
        return f"{reverse_lazy('app')}#/campaign/{self.pk}"

    def get_shopify_proxy_url(self):
        if self.shopify_id:
            return reverse_lazy('proxy:products:product', args=[self.shopify_id])

    def get_prefixed_shopify_proxy_url(self):
        if self.shopify_id:
            return f'{settings.SHOPIFY_PROXY_PREFIX}{self.get_shopify_proxy_url()}'

    def get_proxy_url(self):
        if self.is_published and self.shopify_id:
            return f'https://{settings.SHOPIFY_STORE_URL}{self.get_prefixed_shopify_proxy_url()}'

    def get_affiliate_url(self):
        if self.platform == Platform.AMAZON.name:
            return self.get_amazon_keywords_url(self.affiliate_link, self.keywords)
        else:
            return self.affiliate_link

    @staticmethod
    def get_amazon_keywords_url(url, keywords_list):
        if keywords_list:
            return '?'.join([url, urlencode({'keywords': keywords_list})])
        else:
            return url

    def get_affiliate_redirect_url(self):
        return reverse_lazy('proxy:products:redirect', args=[self.shopify_id])

    @property
    def image_url_local(self):
        img = self.images.first()
        if img:
            return img.img.url

    def image_url(self):
        img = self.images.first()
        if img:
            return img.src

    # Statuses

    def is_ended(self):
        return self.status == Status.ENDED.name

    def is_archived(self):
        return self.status == Status.ARCHIVED.name

    def is_editable(self):
        return self.is_draft()

    def is_approved(self):
        return self.status == Status.APPROVED.name

    def is_draft(self):
        return self.status == Status.DRAFT.name

    @property
    def is_paid(self):
        return self.status == Status.PAID.name

    @property
    def is_published(self):
        return bool(self.published_at)

    def is_ready_for_review(self):
        return self.is_draft()

    def is_charged(self):
        try:
            return bool(self.stripe_charge)
        except ObjectDoesNotExist:
            return False

    def is_ready_for_checkout(self):
        return all([
            self.status == Status.APPROVED.name,
            not self.is_charged(),
        ])

    @property
    def is_active(self):
        return self.status == Status.ACTIVE.name

    @property
    def is_started(self):
        return self.date_start <= timezone.now()

    def is_promo_code_applicable(self):
        return self.status == Status.APPROVED.name

    @property
    def is_status_changed(self):
        return self.status != self._status

    def get_promo_code(self):
        if self.promo_code:
            return self.promo_code.code

    @property
    def factor(self):
        return self.reward / 100

    @property
    def default_variant(self):
        return self.variants.get()

    @property
    def price_after_reward(self):
        """Return product price after reward subtraction"""
        return round_decimal(self.price - self.price * self.factor)

    @property
    def days(self):
        return math.ceil((self.date_end - self.date_start).total_seconds() / (60 * 60 * 24))

    @property
    def days_by_now(self):
        return max([0, math.ceil((timezone.now() - self.date_start).total_seconds() / (60 * 60 * 24))])

    @property
    def sold_count(self):
        return self.orders.filter(approved_by_merchant=True).count()

    @property
    def unsold_count(self):
        return self.total_qty - self.sold_count

    @property
    def refund_rewards(self):
        return round_decimal(self.price * self.unsold_count * self.factor)

    @property
    def rewards(self):
        """
        Returns total possible amount of all products rewards based on daily limit

        Returns:
            decimal.Decimal
        """
        return round_decimal(self.price * self.total_qty * self.factor)

    def get_reward(self):
        return round_decimal(self.price * self.factor)

    @property
    def rewards_by_now(self):
        """
        Returns total amount of all products rewards for approved orders by now;
        these rewards are not necessary paid

        Returns:
            decimal.Decimal
        """
        return round_decimal(self.price * self.sold_count * self.factor)

    @cached_property
    def refund_goal(self):
        """
        Returns amount of unsold products rewards and Steebie fee

        Returns:
            decimal.Decimal
        """
        fee = self.unsold_count * settings.STEEBIE_FEE
        if self.promo_code:
            fee *= self.promo_code.factor
        return self.refund_rewards + fee

    @property
    def refund_amount(self):
        """
        Returns total refund amount (including Stripe fee if it a full refund)

        Returns:
            decimal.Decimal
        """

        if self.is_paid or self.is_published:
            if self.sold_count == 0:
                return self.stripe_charge.amount
            else:
                return self.refund_goal
        else:
            return Decimal('0.0')

    @property
    def total_qty(self):
        """
        Returns total possible amount of product items can be sold during campaign period

        Returns:
            int
        """
        return self.days * self.daily_limit

    @property
    def steebie_fee_full(self):
        """Return Steebie fee without discount applied"""
        return round_decimal(self.total_qty * settings.STEEBIE_FEE)

    @property
    def steebie_fee_full_by_now(self):
        """Return Steebie fee amount for sold products without discount applied"""
        return round_decimal(self.sold_count * settings.STEEBIE_FEE)

    @property
    def steebie_fee(self):
        """Return Steebie fee with discount applied"""
        fee = self.steebie_fee_full
        if self.promo_code:
            fee = fee - fee * self.promo_code.factor

        return fee

    @property
    def steebie_fee_by_now(self):
        """Return Steebie fee amount for sold products with discount applied"""
        fee = self.steebie_fee_full_by_now
        if self.promo_code:
            fee = fee - fee * self.promo_code.factor

        return round_decimal(fee)

    @property
    def promo_code_savings(self):
        return self.steebie_fee_full - self.steebie_fee

    @property
    def stripe_fee(self):
        """Calcs Stripe fee in base currency

        See: https://support.stripe.com/questions/charging-stripe-fees-to-customers

        Returns:
            decimal.Decimal
        """
        return self.total_charge - self.rewards - self.steebie_fee

    @property
    def charge_goal(self):
        """
        Returns charge goal amount (w/o Stripe fee) in base currency

        Returns:
            decimal.Decimal
        """
        return self.rewards + self.steebie_fee

    @property
    def total_charge(self):
        """
        Returns total charge amount (including all fees) in base currency

        Returns:
            decimal.Decimal
        """
        return round_decimal((self.charge_goal + STRIPE_FEE_FIXED) / (1 - STRIPE_FEE_PERCENT))

    def get_charge_description(self):
        return (
            f'Rewards: {self.rewards}, '
            f'Steebie fee: {self.steebie_fee}, '
            f'Stripe fee: {self.stripe_fee}, '
            f'Total: {self.total_charge}'
        )

    def mark_paid(self):
        self.status = Status.PAID.name
        self.save()

        publish_product.delay(self.pk)

    def make_refund(self):
        if self.sold_count == 0:
            StripeRefund.create(charge=self.stripe_charge)
        else:
            if self.refund_goal > 0:
                StripeRefund.create(charge=self.stripe_charge, amount=self.refund_goal)

    def apply_promo_code(self, promo_code):
        """
        Check and apply promo code

        Args:
            promo_code (PromoCode):

        Returns:
            None
        """

        if promo_code.discount_limit and (self.steebie_fee_full * promo_code.factor) > promo_code.discount_limit:
            raise ValidationError(f'Promo code discount is limited to {promo_code.discount_limit}.')

        self.promo_code = promo_code
        self.save()

    @set_shopify_site
    def sync(self, **kwargs):
        # Add auto tags before sync (model won't be saved, it's only for Shopify)
        self._set_auto_tags()

        resource = super().sync(**kwargs)

        # Add product to private custom collection
        if self.private:
            private_collection = CustomCollection.get_private()
            if private_collection:
                if not private_collection.get_shopify_id():
                    private_collection.sync()
            else:
                # Create private collection if not exists
                private_collection = CustomCollection.create_private()

            # Add product to private collection
            collect, created = self.collects.get_or_create(collection=private_collection)
            if not collect.get_shopify_id():
                collect.sync()

        return resource

    # Metafields

    def _get_meta_cashback(self):
        return {
            'key': 'cashback',
            'value_type': 'string',
            # JSON serializable
            'value': str(self.reward)
        }

    def _get_meta_end_date(self):
        return {
            'key': 'end_date',
            'value_type': 'string',
            # JSON serializable
            'value': str(self.date_end)
        }

    def _get_meta_proxy_url(self):
        return {
            'key': 'proxy_url',
            'value_type': 'string',
            'value': f'{self.get_prefixed_shopify_proxy_url()}'
        }

    def get_metafields(self):
        return [
            self._get_meta_cashback(),
            self._get_meta_end_date(),
            self._get_meta_proxy_url(),
        ]

    # Tags & keywords

    def _get_price_tag(self):
        steps = [
            0,
            1,
            10,
            25,
            50,
            100,
            200,
        ]

        price = self.default_variant.price
        tag = None
        for i, n in enumerate(steps):
            if price < n:
                tag = f'd{steps[i - 1]}-{n}'
                break

        if not tag:
            tag = 'd200a'

        if price < 25:
            tag = 'ud25'

        return tag

    def _get_cashback_tag(self):
        steps = [
            50,
            60,
            70,
            80,
            90,
            100,
        ]

        for i, n in enumerate(steps):
            if n <= self.reward < steps[i + 1]:
                return f'p{n}'

    def _set_auto_tags(self):
        tags = []

        # Price
        price_tag = self._get_price_tag()
        if price_tag:
            tags.append(price_tag)

        # Cashback
        cashback_tag = self._get_cashback_tag()
        if cashback_tag:
            tags.append(cashback_tag)

        # Upcoming
        if not self.is_started:
            tags.append('upcoming')

        if tags:
            tags.extend(self.tags_list)

        self.shopify_tags = ','.join(filter(bool, tags))

    @property
    def keywords_list(self):
        return self.keywords.split()

    @keywords_list.setter
    def keywords_list(self, value):
        self.keywords = ' '.join(value)

    @property
    def tags_list(self):
        return self.tags.split(',')

    @tags_list.setter
    def tags_list(self, value):
        self.tags = ','.join(value)

    @property
    def shopify_tags(self):
        return self._shopify_tags

    @shopify_tags.setter
    def shopify_tags(self, value):
        """Prevent tags update from Shopify"""
        self._shopify_tags = value

    @property
    def label(self):
        mapping = {
            'hot_deal': _('HOT DEAL'),
            'last_chance': _('LAST CHANCE'),
            'trending': _('TRENDING'),
            'new': _('NEW'),
            'top_seller': _('TOP SELLER'),
        }

        for k, label in mapping.items():
            if k in self.tags.split():
                return label
        # Return default
        return mapping['hot_deal']

    @property
    def label_class(self):
        mapping = [
            'hot_deal',
            'last_chance',
            'trending',
            'new',
            'top_seller',
        ]

        for label in mapping:
            if label in self.tags.split():
                return label.replace('_', '-')
        # Return default
        return 'hot-deal'

    def delete_product(self, sync=True):
        # Just delete the product if it's not paid (it's a draft)
        if self.status in Status.drafts():
            self.delete()
        else:
            # Otherwise mark it as archived and remove from Shopify
            if self.is_published and sync:
                try:
                    resource = self.get_resource()
                    if resource:
                        resource.destroy()
                        self.published_at = None
                except pyactiveresource.connection.Error as e:
                    if settings.DEBUG:
                        log.warning(e)
                        self.published_at = None
                    else:
                        raise

            if self.is_fake:
                self.delete()

            if self.orders.pending_approval().exists():
                status = Status.ENDED.name
            else:
                status = Status.ARCHIVED.name

            self.status = status
            self.published_at = None
            self.save()

            if self.is_status_changed:
                if self.is_ended():
                    self.send_email_ended()
                elif self.is_archived():
                    self.make_refund()
                    self.send_email_archived()

    def review_product(self):
        self.review = Review.APPROVED.name
        self.status = Status.APPROVED.name
        self.save()

        return self.review

    def get_on_hold_count(self):
        """
        Return count of pending/approved orders for the last 24 hours
        and count of affiliate link clicks for the hold period

        Returns:
            int
        """
        count = 0

        # Orders
        midnight = timezone.datetime.today().replace(hour=0, minute=0, second=0, microsecond=0).replace(tzinfo=utc)
        count += self.orders.filter(
            date_created__gte=midnight,
            status__in=(OrderStatus.PENDING.name, OrderStatus.APPROVED.name)
        ).count()

        # Clicks
        count += self.clicks.filter(
            date_created__gte=(
                timezone.now() - timezone.timedelta(seconds=settings.PRODUCT_HOLD_PERIOD)
            ).replace(tzinfo=utc)
        ).count()

        return count

    def check_limit(self):
        """
        Check if the number of product orders exceeded the daily limit;
        publish/unpublish the product depending on it

        Returns:
            None
        """

        is_changed = False
        if self.daily_limit <= self.get_on_hold_count():
            if self.published_at is not None:
                self.published_at = None
                is_changed = True
        else:
            if self.published_at is None:
                self.published_at = timezone.now()
                is_changed = True

        if is_changed:
            self.save()
            sync_product.delay(self.pk, nested=False)

    def confirm_order(self, customer, order_num):
        if self.orders.filter(customer=customer).exists():
            raise ValidationError(_('You have already confirmed this product.'))

        if Order.objects.filter(order_num=order_num).exists():
            raise ValidationError(_('This order number has already been submitted.'))

        with atomic():
            # Create order
            order = self.orders.create(customer=customer, order_num=order_num, status=OrderStatus.PENDING.name)
            # Remove link click object
            click = self.clicks.filter(customer=customer).first()
            if click:
                click.delete()

            return order

    # Emails

    def send_email_archived(self):
        email = create_html_email(
            template='emails/campaign/archived.html',
            context={
                'object': self,
                'name': self.user.get_full_name(),
            },
            subject='Steebie | Product Campaign Report',
            to=[self.user.email]
        )
        email.send(fail_silently=True)

    def send_email_ended(self):
        email = create_html_email(
            template='emails/campaign/ended.html',
            context={
                'object': self,
                'name': self.user.get_full_name(),
                'orders_count': self.orders.pending_approval().count(),
                'domain': Site.objects.get_current().domain,
            },
            subject='Steebie | Your Product Campaign Ended',
            to=[self.user.email]
        )
        email.send(fail_silently=True)

    # Stats

    @cached_property
    def stats(self):
        pending_orders_qs = self.orders.pending_approval()
        dt_start = timezone.now() - timezone.timedelta(days=1)

        data = {
            'sold_count': self.sold_count,
            'unsold_count': self.unsold_count,
            'rewards_by_now': self.rewards_by_now,
            'steebie_fee_by_now': self.steebie_fee_by_now,
            'days_by_now': self.days_by_now,
            'orders_pending': pending_orders_qs.count(),
            'orders_new': pending_orders_qs.filter(
                date_created__gte=dt_start.replace(hour=0, minute=0, second=0, microsecond=0, tzinfo=utc)
            ).count(),
            'ga': False,
        }

        if Analytics.enabled() and self.shopify_id:
            analytics = Analytics()
            data.update({
                'ga': True,
                'views': analytics.unique_pageviews(self.get_prefixed_shopify_proxy_url()),
                'clicks': analytics.unique_clicks(self.get_prefixed_shopify_proxy_url())
            })

        return data


class Variant(BaseShopify):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='variants', unique=True)
    shopify_id = models.BigIntegerField(null=True, blank=True)
    price = models.DecimalField(max_digits=9, decimal_places=2)
    compare_at_price = models.DecimalField(
        _('Price before sale'), max_digits=9, decimal_places=2, null=True, blank=True
    )

    shopify_fields_map = {
        'id': 'shopify_id'
    }

    shopify_fields = [
        'id',
        'price',
        'compare_at_price'
    ]

    shopify_resource_cls = shopify.Variant
    shopify_parent = 'product'
    shopify_parent_id_name = 'product_id'

    def __str__(self):
        return str(self.product)

    def as_shopify_resource(self):
        variant = super().as_shopify_resource()
        if not variant.id:
            # Set default variant ID
            variant.id = self.product.get_resource().variants[0].id
        return variant


class Image(BaseShopify):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='images', null=True, blank=True)
    shopify_id = models.BigIntegerField(null=True, blank=True)
    src = models.CharField(max_length=255, null=True, blank=True)
    img = models.ImageField(upload_to='products/')
    date_created = models.DateTimeField(auto_now_add=True)

    shopify_fields_map = {
        'id': 'shopify_id'
    }

    shopify_fields = [
        'id',
        'src',
    ]

    shopify_resource_cls = shopify.Image
    shopify_parent = 'product'
    shopify_parent_id_name = 'product_id'

    def __str__(self):
        return f'{str(self.product)}: {str(self.img)}'

    def as_shopify_resource(self):
        image = super().as_shopify_resource()
        image.attach_image(self.img.read(), filename=os.path.basename(self.img.name))
        return image


class PromoCodeQuerySet(QuerySet):

    def active(self):
        return self.exclude(
            Q(expiration_date__lt=timezone.now())
        ).annotate(
            usage_count=Count('products')
        ).filter(
            Q(usage_count__lt=F('usage_limit')) | Q(usage_limit__isnull=True)
        )

    def search(self, code):
        return self.active().get(code__iexact=code)


class PromoCode(models.Model):
    code = models.CharField(max_length=255, unique=True, help_text=_('Unique, case-insensitive'))
    discount_percent = models.DecimalField(
        max_digits=5, decimal_places=2, help_text=_('From 0.01 to 100.0'), validators=[promo_limits]
    )
    discount_limit = models.DecimalField(
        max_digits=9, decimal_places=2, help_text=_('Max discount amount'), null=True, blank=True
    )
    usage_limit = models.PositiveIntegerField(help_text=_('How much times it can be applied'), null=True, blank=True)
    expiration_date = models.DateTimeField(null=True, blank=True)

    objects = PromoCodeQuerySet.as_manager()

    def __str__(self):
        return self.code

    @property
    def factor(self):
        return self.discount_percent / 100


class FakeUpload(models.Model):
    f = models.FileField(upload_to='fakes/', verbose_name='File')
    task_id = models.CharField(max_length=255, null=True, blank=True, editable=False)
    date_started = models.DateTimeField(auto_now_add=True)
    date_ended = models.DateTimeField(null=True, blank=True, editable=False)
    is_successful = models.NullBooleanField(editable=False)
    uploaded_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, editable=False)

    def __str__(self):
        return self.f.path.split('/')[-1]
