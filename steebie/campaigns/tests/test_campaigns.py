import random
from contextlib import ExitStack
from unittest import mock
from unittest.mock import Mock

from django.core import mail

import pytest
from django.utils import timezone
from pyactiveresource.activeresource import ActiveResource

from campaigns.models import Status, Product
from orders.models import Order
from payments.models import StripeRefund
from taskapp.celery import end_or_archive_products


@pytest.mark.django_db(transaction=True)
def test_campaign_end(product, orders_pending):
    product.status = Status.PUBLISHED.name
    product.save()

    Order.objects.filter(pk__in=[o.pk for o in orders_pending]).update(product=product)

    assert product.orders.pending_approval().exists()

    with ExitStack() as stack:
        patches = [
            mock.patch('campaigns.models.Product.get_resource', return_value=Mock(spec=ActiveResource)),
            mock.patch('pyactiveresource.activeresource.ActiveResource.destroy', return_value=None),
        ]
        for p in patches:
            stack.enter_context(p)

        product.delete_product()

    product.refresh_from_db()

    assert product.orders.pending_approval().exists()
    assert product.status == Status.ENDED.name
    assert StripeRefund.objects.all().count() == 0
    assert len(mail.outbox) == 1
    assert mail.outbox[0].subject == 'Steebie | Your Product Campaign Ended'


@pytest.mark.django_db(transaction=True)
def test_campaign_archive(product, orders_completed, stripe_charge, stripe_refund_create):
    product.status = Status.PUBLISHED.name
    product.save()

    stripe_charge.product = product

    Order.objects.filter(pk__in=[o.pk for o in orders_completed]).update(product=product)

    assert not product.orders.pending_approval().exists()

    with ExitStack() as stack:
        patches = [
            mock.patch('campaigns.models.Product.get_resource', return_value=Mock(spec=ActiveResource)),
            mock.patch('pyactiveresource.activeresource.ActiveResource.destroy', return_value=None),
            mock.patch('stripe.Refund.create', side_effect=stripe_refund_create),
        ]
        for p in patches:
            stack.enter_context(p)

        product.delete_product()

    product.refresh_from_db()

    assert not product.orders.pending_approval().exists()
    assert product.status == Status.ARCHIVED.name
    assert StripeRefund.objects.all().count() == 1
    assert StripeRefund.objects.get().product == product
    assert StripeRefund.objects.get().charge == stripe_charge
    assert len(mail.outbox) == 2
    assert mail.outbox[0].subject == '[TX][REV] Stripe Refund'
    assert mail.outbox[1].subject == 'Steebie | Product Campaign Report'


@pytest.mark.django_db(transaction=True)
def test_campaign_end_or_archive(product, orders_pending, stripe_charge, stripe_refund_create):
    product.status = Status.PUBLISHED.name
    product.date_end = timezone.now()
    product.save()

    product_published = Product.objects.get(pk=product.pk)
    product_published.pk = None
    product_published.shopify_id = random.randrange(100000, 999999)
    product_published.date_end = timezone.now() + timezone.timedelta(hours=1)
    product_published.save()

    product_ended = Product.objects.get(pk=product.pk)
    product_ended.pk = None
    product_ended.shopify_id = random.randrange(100000, 999999)
    product_ended.status = Status.ENDED.name
    product_ended.date_end = timezone.now() + timezone.timedelta(days=1)
    product_ended.save()

    product_ended_pending = Product.objects.get(pk=product.pk)
    product_ended_pending.pk = None
    product_ended_pending.shopify_id = random.randrange(100000, 999999)
    product_ended_pending.status = Status.ENDED.name
    product_ended.date_end = timezone.now() + timezone.timedelta(days=1)
    product_ended_pending.save()

    stripe_charge.product = product_ended
    stripe_charge.save()

    orders_half = int(len(orders_pending) / 2)
    Order.objects.filter(pk__in=[o.pk for o in orders_pending[:orders_half]]).update(product=product)
    Order.objects.filter(
        pk__in=[o.pk for o in orders_pending[orders_half:]]
    ).update(product=product_ended_pending)

    assert product.orders.pending_approval().exists()
    assert not product_ended.orders.pending_approval().exists()
    assert product_ended_pending.orders.pending_approval().exists()

    with ExitStack() as stack:
        patches = [
            mock.patch('campaigns.models.Product.get_resource', return_value=Mock(spec=ActiveResource)),
            mock.patch('pyactiveresource.activeresource.ActiveResource.destroy', return_value=None),
            mock.patch('stripe.Refund.create', side_effect=stripe_refund_create),
        ]
        for p in patches:
            stack.enter_context(p)

        end_or_archive_products.delay()

    product.refresh_from_db()
    product_published.refresh_from_db()
    product_ended.refresh_from_db()
    product_ended_pending.refresh_from_db()

    assert product.status == Status.ENDED.name
    assert product_published.status == Status.PUBLISHED.name
    assert product_ended.status == Status.ARCHIVED.name
    assert product_ended_pending.status == Status.ENDED.name
    assert StripeRefund.objects.all().count() == 1
    assert StripeRefund.objects.get().product == product_ended
    assert StripeRefund.objects.get().charge == stripe_charge
    assert len(mail.outbox) == 3
    assert mail.outbox[0].subject == 'Steebie | Your Product Campaign Ended'
    assert mail.outbox[1].subject == '[TX][REV] Stripe Refund'
    assert mail.outbox[2].subject == 'Steebie | Product Campaign Report'


@pytest.mark.django_db(transaction=True)
def test_campaign_already_ended_or_archived(product, orders_pending):
    product_archived = product
    product_archived.pk = None
    product_archived.shopify_id = random.randrange(100000, 999999)
    product_archived.status = Status.ARCHIVED.name
    product_archived.date_end = timezone.now() + timezone.timedelta(days=1)
    product_archived.save()

    product_ended_pending = Product.objects.get(pk=product.pk)
    product_ended_pending.pk = None
    product_ended_pending.shopify_id = random.randrange(100000, 999999)
    product_ended_pending.status = Status.ENDED.name
    product_archived.date_end = timezone.now() + timezone.timedelta(days=1)
    product_ended_pending.save()

    Order.objects.filter(pk__in=[o.pk for o in orders_pending]).update(product=product_ended_pending)

    assert not product_archived.orders.pending_approval().exists()
    assert product_ended_pending.orders.pending_approval().exists()

    end_or_archive_products.delay()

    product_archived.refresh_from_db()
    product_ended_pending.refresh_from_db()

    assert product_archived.status == Status.ARCHIVED.name
    assert product_ended_pending.status == Status.ENDED.name
    assert StripeRefund.objects.all().count() == 0
    assert len(mail.outbox) == 0
