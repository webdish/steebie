from decimal import Decimal

from django.urls import reverse

import pytest

from base.factories import MERCHANT_PASSWORD
from campaigns.models import Product, Variant, Image, Status


@pytest.mark.skip(reason='Amazon URL check returns 503')
@pytest.mark.django_db(transaction=True)
def test_product_prices(client, merchant, product_image, product_data):
    product_data['price'] = Decimal('100')
    product_data['reward'] = Decimal('25')
    product_data['images'] = [product_image.id]

    client.login(username=merchant.username, password=MERCHANT_PASSWORD)
    response = client.post(reverse("api:products:list"), product_data, content_type='application/json')

    assert response.status_code == 201
    assert Product.objects.count() == 1
    assert Variant.objects.count() == 1
    assert Image.objects.count() == 1

    product = Product.objects.get()
    variant = Variant.objects.get()
    assert variant.price == product.price_after_reward
    assert variant.compare_at_price == product.price


@pytest.mark.django_db(transaction=True)
def test_product_prices_update(client, product):
    product.status = Status.DRAFT.name
    product.save()

    client.login(username=product.user.username, password=MERCHANT_PASSWORD)
    response = client.post(
        reverse("api:products:product", args=[product.id]),
        {
            'price': product.price * 2
        },
        content_type='application/json'
    )

    assert response.status_code == 200

    product_instance = Product.objects.get()
    variant = Variant.objects.get()
    assert product.price != product_instance.price
    assert variant.price == product_instance.price_after_reward
    assert variant.compare_at_price == product_instance.price
