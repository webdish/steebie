import logging

from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.conf import settings
from django.db.transaction import atomic

import PIL.Image
import rest_framework.exceptions
from rest_framework.exceptions import PermissionDenied, ParseError, NotFound
from rest_framework.generics import ListAPIView
from rest_framework.parsers import MultiPartParser, JSONParser
from rest_framework.response import Response

from base.auth import IsMerchant
from base.exceptions import ServiceUnavailable
from base.pagination import StandardResultsSetPagination
from base.views.api import BaseAPIResource, BaseAPIObjectResource
from campaigns.serializers import (
    ProductSerializer,
    StripeCheckoutSerializer,
    OrderSerializer,
    OrdersConfirmSerializer,
    ProductStatusSerializer,
    OrderStatusSerializer,
)
from orders.models import Order, OrderStatus
from payments.models import StripeCharge
from campaigns.models import PromoCode, Status
from campaigns.models import Product, Image
from taskapp.celery import delete_product

log = logging.getLogger(__name__)


class ProductsResource(BaseAPIResource):

    model = Product

    def get_queryset(self, **kwargs):
        return super().get_queryset(user=self.request.user, is_fake=False, **kwargs)

    def get(self, request):
        kwargs = {}
        serializer = ProductStatusSerializer(data=request.query_params)
        serializer.is_valid(raise_exception=True)
        if 'status' in serializer.validated_data:
            kwargs = {'status': serializer.validated_data['status']}

        return Response(data=ProductSerializer(self.get_queryset(**kwargs), many=True).data)

    def post(self, request):
        serializer = ProductSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        return Response(status=201, data=ProductSerializer(serializer.save(user=request.user)).data)


class BaseProductResource(BaseAPIObjectResource):

    def get_object(self, qs=None):
        instance = super().get_object(qs=qs)
        if instance.user != self.request.user:
            raise PermissionDenied
        if instance.is_fake:
            raise PermissionDenied

        return instance


class ProductResource(BaseProductResource):

    model = Product

    def get(self, request, **kwargs):
        return Response(data=ProductSerializer(self.get_object()).data)

    def post(self, request, **kwargs):
        instance = self.get_object()

        if not instance.is_editable():
            raise ParseError(detail=f'Product {instance.pk} is not editable.')

        # Update instance
        serializer = ProductSerializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        if len(serializer.validated_data) < 1:
            raise ParseError(detail='Nothing to update.')

        # Save instance
        instance = serializer.save()

        return Response(status=200, data=ProductSerializer(instance).data)

    def delete(self, request, **kwargs):
        instance = self.get_object()

        if instance.is_archived() or instance.is_ended():
            raise ParseError(detail=f'Product {instance.pk} is ended/archived.')

        delete_product.delay(instance.pk)

        return Response(status=202, data={'refund': instance.refund_amount})


class ProductReviewResource(BaseProductResource):

    model = Product

    def get_object(self, qs=None):
        instance = super().get_object(qs=qs)
        if not instance.is_ready_for_review():
            raise ParseError(detail='Product is not ready for review.')

        return instance

    def post(self, request, **kwargs):
        return Response(data={'status': self.get_object().review_product()})


class ProductDiscountResource(BaseProductResource):

    model = Product

    def get_object(self, qs=None):
        instance = super().get_object(qs=qs)
        if not instance.is_promo_code_applicable():
            raise ParseError(detail=f'Promo code cannot be applied to the campaign.')

        return instance

    def post(self, request, **kwargs):
        instance = self.get_object()

        try:
            promo_code = PromoCode.objects.search(code=request.data['code'])
            instance.apply_promo_code(promo_code=promo_code)

            return Response(data={
                'fee_discounted': instance.steebie_fee,
                'fee_full': instance.steebie_fee_full,
                'savings': instance.promo_code_savings
            })
        except KeyError:
            raise ParseError({'code': 'Field is required.'})
        except ObjectDoesNotExist:
            raise NotFound
        except ValidationError as e:
            # Catching Django ValidationError, not DRF
            raise rest_framework.exceptions.ValidationError(detail={'code': e.messages})

    def delete(self, request, **kwargs):
        instance = self.get_object()
        instance.promo_code = None
        instance.save()

        return Response(data={'fee': instance.steebie_fee})


class ProductCheckoutResource(BaseProductResource):

    model = Product

    def get_object(self, qs=None):
        instance = super().get_object(qs=qs)
        if not instance.is_ready_for_checkout():
            raise ParseError(detail='Product is not ready for checkout.')

        return instance

    def get(self, request, **kwargs):
        instance = self.get_object()
        data = {
            'description': instance.get_charge_description(),
            'key': settings.STRIPE_PUBLISHABLE_KEY,
            'currency': settings.STRIPE_DEFAULT_CURRENCY,
            'amount_base': instance.total_charge,
            'amount_smallest': StripeCharge.get_stripe_amount(
                instance.total_charge,
                settings.STRIPE_DEFAULT_CURRENCY
            ),
            'email': self.request.user.email
        }

        return Response(data=data)

    def post(self, request, **kwargs):
        instance = self.get_object()

        serializer = StripeCheckoutSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Create charge
        charge = StripeCharge.create(token=serializer.validated_data['token'], product=instance)

        if charge:
            instance.mark_paid()
            return Response(data={
                'amount': charge.amount,
                'currency': charge.currency,
                'receipt_url': charge.receipt_url
            })
        else:
            raise ServiceUnavailable()


class ProductPriceBreakdownResource(BaseProductResource):

    model = Product

    def get(self, request, **kwargs):
        instance = self.get_object()

        return Response(data={
            'total_products': instance.total_qty,
            'total_rewards': instance.rewards,
            'total_fee': instance.steebie_fee,
            'total': instance.total_charge,
            'fee': settings.STEEBIE_FEE,
            'days': instance.days,
            'daily_limit': instance.daily_limit,
            'reward': instance.get_reward(),
            'promo_code': instance.get_promo_code(),
            'savings': instance.promo_code_savings,
        })


class ProductStatsResource(BaseProductResource):

    model = Product

    def get(self, request, **kwargs):
        instance = self.get_object()

        return Response(data={
            'sold': instance.sold_count,
            'unsold': instance.unsold_count,
            'rewards_paid': instance.rewards_by_now,
            'steebie_fee_paid': instance.steebie_fee_by_now,
            'duration': instance.days_by_now,
            'title': instance.title,
        })


class ProductImage(BaseAPIResource):

    parser_classes = (MultiPartParser,)
    model = Image

    SIZE_MAX = 4472
    FILE_SIZE_MAX = 20971520

    def get_queryset(self):
        return self.model.objects.none()

    def _check_image(self, image_data):
        image = PIL.Image.open(image_data)
        width, height = image.size

        if width != height:
            raise rest_framework.exceptions.ValidationError({'file': 'Image ratio must be 1:1.'})

        if width > self.SIZE_MAX:
            raise rest_framework.exceptions.ValidationError(
                {'file': f'Any image side must not exceed {self.SIZE_MAX} pixels.'}
            )

    def post(self, request):
        if 'file' in request.data and isinstance(request.data['file'], InMemoryUploadedFile):
            self._check_image(request.data['file'])
            with atomic():
                instance = Image.objects.create(img=request.data['file'])
                if instance.img.size > self.FILE_SIZE_MAX:
                    raise rest_framework.exceptions.ValidationError(
                        {'file': f'Image file size must not exceed {self.FILE_SIZE_MAX} bytes.'}
                    )
            return Response(status=201, data={'id': instance.pk, 'url': instance.img.url})
        else:
            return Response(status=400, data={'detail': "Missing 'file'."})


class OrdersResource(BaseAPIResource, ListAPIView):

    model = Order
    serializer_class = OrderSerializer

    def get_queryset(self, **kwargs):
        kwargs = {
            'product__user': self.request.user
        }
        serializer = OrderStatusSerializer(data=self.request.query_params)
        serializer.is_valid(raise_exception=True)
        if 'status' in serializer.validated_data:
            kwargs.update({'status': serializer.validated_data['status']})

        return super().get_queryset(**kwargs)


class OrdersConfirmationResource(BaseAPIResource):

    model = Order
    permission_classes = (IsMerchant,)

    def post(self, request, **kwargs):
        serializer = OrdersConfirmSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        with atomic():
            data = {}
            for order in self.get_queryset(pk__in=serializer.validated_data['orders']):
                # Check order status and merchant
                if order.product.user != self.request.user:
                    raise PermissionDenied()
                if order.status not in OrderStatus.incomplete():
                    raise rest_framework.exceptions.ValidationError(
                        {'orders': [f'Order {order.pk} is already completed. Cannot change it.']}
                    )

                # Approve
                order.approved_by_merchant = serializer.validated_data['approved']
                order.review_order()
                data[order.pk] = {
                    'approved_by_merchant': order.approved_by_merchant,
                    'approved_by_steebie': order.approved_by_steebie,
                }

            return Response(data=data)


# Public Endpoints

class ProductsListResource(ListAPIView):

    authentication_classes = []
    permission_classes = []
    parser_classes = (JSONParser,)
    serializer_class = ProductSerializer
    pagination_class = StandardResultsSetPagination

    def get_queryset(self):
        return Product.objects.exclude(status=Status.DRAFT.name)
