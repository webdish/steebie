from decimal import Decimal
import hashlib

from django.db import models
from django.db.models import QuerySet, Sum, F, Q
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from base.validators import amazon_order_num
from utils.enums import ChoiceEnum
from utils.mail import create_html_email
from utils.numbers import round_decimal
import base.context


class OrderStatus(ChoiceEnum):
    PENDING = _('Pending')
    APPROVED = _('Approved')
    REJECTED = _('Declined')
    COMPLETED = _('Completed')

    @classmethod
    def incomplete(cls):
        return cls.PENDING.name, cls.APPROVED.name, cls.REJECTED.name


class OrdersQuerySet(QuerySet):
    def review(self, approved_by_merchant):
        if approved_by_merchant is None:
            return 0
        else:
            if approved_by_merchant:
                status = OrderStatus.APPROVED.name
            else:
                status = OrderStatus.REJECTED.name
            return self.update(
                approved_by_merchant=approved_by_merchant,
                approved_by_steebie=approved_by_merchant,
                status=status
            )

    @staticmethod
    def _get_date_hold_end():
        return timezone.now() - timezone.timedelta(days=settings.HOLD_PERIOD_DAYS)

    def auto_approve_ready(self):
        return self.filter(
            date_created__lt=timezone.now() - timezone.timedelta(days=settings.AUTO_APPROVE_PERIOD_DAYS),
            approved_by_merchant__isnull=True,
            approved_by_steebie__isnull=True,
            status=OrderStatus.PENDING.name
        )

    def pending_approval(self):
        """Returns orders not yet reviewed by the merchant"""
        return self.filter(approved_by_merchant__isnull=True, status=OrderStatus.PENDING.name)

    def pending(self):
        """Returns orders not yet reviewed by the merchant or still on hold"""
        return self.filter(
            (Q(approved_by_merchant__isnull=True) & Q(status=OrderStatus.PENDING.name)) |
            (
                Q(approved_by_merchant=True) &
                Q(status=OrderStatus.APPROVED.name) &
                Q(date_created__gt=self._get_date_hold_end())
            )
        )

    def available(self):
        """Returns orders ready for redeem (with status 'APPROVED' and hold period expired)"""
        return self.filter(status=OrderStatus.APPROVED.name, date_created__lte=self._get_date_hold_end())

    def completed(self):
        """Returns completed orders"""
        return self.filter(status=OrderStatus.COMPLETED.name)

    def rewards(self):
        rewards_sum = self.aggregate(
            rewards=Sum(
                F('product__price') * (F('product__reward') / 100),
                output_field=models.DecimalField()
            )
        )['rewards']

        if rewards_sum:
            return round_decimal(rewards_sum)
        return Decimal('0.0')

    def numbers(self):
        return list(map(str, self.values_list('order_num', flat=True)))

    def md5(self):
        return hashlib.md5('.'.join(self.numbers()).encode()).hexdigest()

    def approved(self):
        """Orders approved by the merchant"""
        return self.filter(approved_by_merchant=True)


class Order(models.Model):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._status = self.status

    product = models.ForeignKey(
        'campaigns.Product', to_field='shopify_id', on_delete=models.PROTECT, related_name='orders'
    )
    customer = models.ForeignKey('customers.Customer', on_delete=models.CASCADE, related_name='orders')
    order_num = models.CharField(max_length=19, unique=True, validators=[amazon_order_num])
    approved_by_merchant = models.NullBooleanField()
    approved_by_steebie = models.NullBooleanField()
    status = models.CharField(max_length=255, choices=OrderStatus.choices())
    email_sent = models.BooleanField(default=False)
    date_created = models.DateTimeField(auto_now_add=True)

    objects = OrdersQuerySet.as_manager()

    class Meta:
        unique_together = ('product', 'customer')
        ordering = ['-date_created']

    def __str__(self):
        return str(self.order_num)

    @property
    def status_prev(self):
        return self._status

    @property
    def reward(self):
        return self.product.get_reward()

    @property
    def date_scheduled(self):
        return self.date_created + timezone.timedelta(days=settings.HOLD_PERIOD_DAYS)

    @property
    def is_completed(self):
        return self.status == OrderStatus.COMPLETED.name

    def review_order(self):
        if self.approved_by_merchant is not None:
            self.approved_by_steebie = self.approved_by_merchant
            if self.approved_by_merchant:
                self.status = OrderStatus.APPROVED.name
            else:
                self.status = OrderStatus.REJECTED.name
            self.save()

    def _get_email(self, template):
        context = {
            'product': self.product.title,
            'reward_amount': self.product.get_reward(),
            'name': self.customer.get_full_name(),
            'account_url': reverse_lazy("proxy:account:account"),
        }
        context.update(base.context.settings(None))

        return create_html_email(
            template=template,
            context=context,
            subject='Steebie | CashBack rewards status update',
            to=[self.customer.email]
        )

    def get_email_hold_is_over(self):
        return self._get_email('emails/orders/hold_is_over.html')

    def get_email_status_changed(self):
        return self._get_email(f'emails/orders/status_{self.status.lower()}.html')


class AffiliateLinkClick(models.Model):
    product = models.ForeignKey(
        'campaigns.Product', to_field='shopify_id', on_delete=models.PROTECT, related_name='clicks'
    )
    customer = models.ForeignKey('customers.Customer', on_delete=models.CASCADE, related_name='clicks')
    date_created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['date_created']
        unique_together = ('product', 'customer')

    @classmethod
    def create_if_not_exists(cls, product, customer):
        if not cls.objects.filter(product=product, customer=customer).exists():
            obj = cls(product=product, customer=customer)
            obj.save()
            return obj
