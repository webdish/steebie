from rest_framework.fields import URLField, DecimalField, DateTimeField, CharField
from rest_framework.serializers import ModelSerializer

from campaigns.models import Product
from orders.models import Order


class OrderedProductSerializer(ModelSerializer):
    image = URLField(source='image_url')
    price = DecimalField(max_digits=6, decimal_places=2)

    class Meta:
        model = Product
        fields = [
            'image',
            'price',
            'shopify_id',
            'title',
        ]


class OrderSerializer(ModelSerializer):
    product = OrderedProductSerializer()
    reward = DecimalField(max_digits=6, decimal_places=2)
    date_scheduled = DateTimeField()
    status_name = CharField(source='get_status_display')

    class Meta:
        model = Order
        fields = [
            'product',
            'reward',
            'order_num',
            'status',
            'status_name',
            'date_scheduled',
        ]
