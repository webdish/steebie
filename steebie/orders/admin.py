from django.contrib import admin

from .models import Order, AffiliateLinkClick


class OrderAdmin(admin.ModelAdmin):
    list_display = ['order_num', 'product', 'customer', 'status']


admin.site.register(Order, OrderAdmin)
admin.site.register([AffiliateLinkClick])
