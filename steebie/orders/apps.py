from django.apps import AppConfig


class OrderConfig(AppConfig):
    name = 'orders'

    def ready(self):
        from .signals import order_change, affiliate_click_create, affiliate_click_delete  # noqa 401
