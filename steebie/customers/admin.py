from django.contrib import admin

from .models import Customer, Phone, Sms


class CustomerAdmin(admin.ModelAdmin):

    search_fields = ['id_md5', 'email']


admin.site.register(Customer, CustomerAdmin)
admin.site.register([Phone, Sms])
