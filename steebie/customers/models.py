import random
import warnings
from hashlib import md5
import logging
import datetime

from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.db import models
from django.conf import settings
from django.db.models import QuerySet
from django.utils import timezone
from django.utils.timezone import utc
from django.utils.translation import ugettext_lazy as _

import shopify
from twilio.base.exceptions import TwilioRestException
from twilio.rest import Client

from base.exceptions import PhoneVerificationException
from base.models.shopify import BaseShopify
from base.validators import us_phone_number

log = logging.getLogger(__name__)


class Customer(BaseShopify):
    id = models.BigIntegerField(primary_key=True)
    id_md5 = models.CharField(max_length=32, db_index=True)
    email = models.EmailField(unique=True)
    first_name = models.CharField(max_length=255, null=True, blank=True)
    last_name = models.CharField(max_length=255, null=True, blank=True)
    state = models.CharField(max_length=255)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    shopify_resource_cls = shopify.Customer

    shopify_fields = [
        'id',
        'email',
        'state',
        'first_name',
        'last_name',
        'created_at',
        'updated_at',
    ]

    def save(self, *args, **kwargs):
        force_hash = kwargs.pop('force_hash', False)
        if not self.id_md5 or force_hash:
            self.id_md5 = self.gen_md5()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.email

    def gen_md5(self):
        return md5(f'{self.id}{settings.SHOPIFY_LIQUID_SHARED_SECRET}'.encode('ascii')).hexdigest()

    def get_name(self):
        warnings.warn(
            "get_name is deprecated, use get_full_name instead",
            DeprecationWarning
        )
        return f'{self.first_name} {self.last_name}'

    def get_full_name(self):
        return f'{self.first_name} {self.last_name}'

    def is_stripe_connected(self):
        try:
            return bool(self.stripe_account)
        except ObjectDoesNotExist:
            return False

    def is_phone_verified(self):
        return self.phones.exclude(date_verified__isnull=True).exists()
    is_phone_verified.message = _('Your phone number has already been verified.')

    def is_24h_limit_reached(self):
        if Sms.objects.filter(phone__customer=self).last_24h().count() >= settings.PHONE_VERIFICATION_MAX_RETRIES:
            return True
        return False
    is_24h_limit_reached.message = _('Your 24 hours limit is exceeded.')

    def is_delay_not_passed(self):
        return Sms.objects.filter(phone__customer=self).filter(
            date_sent__gte=(
                datetime.datetime.utcnow().replace(tzinfo=utc) -
                datetime.timedelta(seconds=settings.PHONE_VERIFICATION_REQUESTS_DELAY)
            )
        ).exists()
    is_delay_not_passed.message = _(
        f'Please wait up to {settings.PHONE_VERIFICATION_REQUESTS_DELAY} '
        f'seconds before retrying again.'
    )

    def is_next_code_allowed(self, raise_exception=False):
        check_methods = ['is_phone_verified', 'is_24h_limit_reached', 'is_delay_not_passed']
        for method_name in check_methods:
            method = getattr(self, method_name)
            if method():
                if raise_exception:
                    raise PhoneVerificationException(method.message)
                else:
                    return False
        return True


class Phone(models.Model):
    customer = models.ForeignKey('Customer', related_name='phones', on_delete=models.CASCADE)
    number = models.CharField(max_length=255, validators=[us_phone_number])
    date_verified = models.DateTimeField(null=True, blank=True)

    def clean(self):
        try:
            self.customer.is_next_code_allowed(raise_exception=True)
        except PhoneVerificationException as e:
            raise ValidationError(e)

    @staticmethod
    def generate_code():
        return str(random.randrange(100000, 999999))

    def send_sms(self, code):
        text = f'Steebie verification code: {code}'
        client = Client(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTHTOKEN)
        return client.messages.create(to=self.number, from_=settings.TWILIO_FROM_NUMBER, body=text)

    def send_verification_code(self, raise_exception=False):
        if self.customer.is_next_code_allowed(raise_exception=raise_exception):
            code = self.generate_code()
            try:
                message = self.send_sms(code)
                if message.sid and not message.error_code:
                    return self.sms.create(
                        twilio_id=message.sid,
                        code=code
                    )
                else:
                    if message.error_code:
                        log.error(f'Twilio error {message.error_code}')
            except TwilioRestException as e:
                log.error(e, exc_info=True)

        # If message wasn't send
        if raise_exception:
            raise ValidationError('Error sending code.')


class SmsQuerySet(QuerySet):
    def last_24h(self):
        return self.filter(date_sent__gte=timezone.now() - timezone.timedelta(hours=24))


class Sms(models.Model):
    phone = models.ForeignKey('Phone', related_name='sms', on_delete=models.CASCADE)
    twilio_id = models.CharField(max_length=255)
    code = models.CharField(max_length=6)
    date_sent = models.DateTimeField(auto_now_add=True)

    objects = SmsQuerySet.as_manager()

    def verify(self, code_received):
        dt = timezone.now() - timezone.timedelta(seconds=settings.PHONE_VERIFICATION_CODE_VALIDITY)
        if self.date_sent >= dt and self.code == code_received:
            self.phone.date_verified = timezone.now()
            self.phone.save()
            return True
        return False
