from django.urls import path

from .views import (
    CustomerCreatedWebhookView,
    CustomerDeletedWebhookView,
    CustomerStateChangedWebhookView,
    CustomerUpdatedWebhookView
)

webhooks_customers = [
    path(r'created', CustomerCreatedWebhookView.as_view(), name='created'),
    path(r'deleted', CustomerDeletedWebhookView.as_view(), name='deleted'),
    path(r'disabled', CustomerStateChangedWebhookView.as_view(), name='disabled'),
    path(r'enabled', CustomerStateChangedWebhookView.as_view(), name='enabled'),
    path(r'updated', CustomerUpdatedWebhookView.as_view(), name='updated'),
]
