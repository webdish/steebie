import logging

from django.db.transaction import atomic
from rest_framework.response import Response

from .models import Customer
from base.webhooks import GenericWebhookView, GenericObjectWebhookView, GenericUpdatedWebhookView

log = logging.getLogger(__name__)


class CustomerCreatedWebhookView(GenericWebhookView):
    def post(self, request):
        log.debug(f'Recieved customer creation webhook; webhook data: {request.data}')
        if Customer.objects.filter(pk=request.data.get('id')).exists():
            log.warning(f'Customer {request.data["id"]} is already exists')
            # Shopify will continue to send requests if code is not 200
            return Response(status=200)

        try:
            with atomic():
                customer = Customer.from_json(request.data)
                log.debug(f'Customer {customer.pk} created')
            return Response(status=200)
        except Exception as e:
            log.error(e, exc_info=True)
            return Response(status=500)


class CustomerDeletedWebhookView(GenericObjectWebhookView):

    model = Customer

    def post(self, request):
        log.debug(f'Recieved customer deletion webhook; webhook data: {request.data}')
        customer = self.get_object()
        # Disable customer instead of delete
        customer.state = 'disabled'
        customer.save()
        return Response(status=200)


class CustomerStateChangedWebhookView(GenericUpdatedWebhookView):

    model = Customer
    fields_to_update = ['state', 'updated_at']


class CustomerUpdatedWebhookView(GenericUpdatedWebhookView):

    model = Customer
