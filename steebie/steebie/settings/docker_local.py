import socket

from .base import *  # noqa

# GENERAL

SECRET_KEY = os.getenv('DJANGO_SECRET_KEY', '4JR6LzvWpFDaJ13G7IV6FlcP1fMU4skBa5IzCj39lERH2zmOrJrScD80xkhmqyev')

ALLOWED_HOSTS = ['*']

SKIP_APP_PROXY_VALIDATION = True


# Logging

DEFAULT_LOGGER_CONF = {
    'handlers': ['console', 'file'],
    'level': 'DEBUG',
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'propagate': False,
    'root': DEFAULT_LOGGER_CONF,
    'formatters': {
        'verbose': {
            'format': '[%(asctime)s] %(levelname)s - p%(process)s {%(filename)s:%(lineno)d} %(message)s',
        }
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
        'file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(LOGS_DIR, 'django-debug.log'),
            'maxBytes': 1000 * 1000 * 10,  # 10MB
            'backupCount': 10,
            'formatter': 'verbose'
        },
        'templates': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(LOGS_DIR, 'django-templates.log'),
            'maxBytes': 1000 * 1000 * 10,  # 10MB
            'backupCount': 10,
            'formatter': 'verbose'
        },
        'celery': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(LOGS_DIR, 'celery-debug.log'),
            'maxBytes': 1000 * 1000 * 10,  # 10MB
            'backupCount': 10,
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django.db.backends': {
            'level': 'ERROR',
            'handlers': ['console', 'file'],
            'propagate': False,
        },
        'django.request': {
            'level': 'WARNING',
            'handlers': ['console', 'file'],
            'propagate': False,
        },
        'django.server': {
            'level': 'WARNING',
            'handlers': ['console', 'file'],
            'propagate': False,
        },
        'django.template': {
            'level': 'DEBUG',
            'handlers': ['templates'],
            'propagate': False,
        },
        'celery': {
            'level': 'DEBUG',
            'handlers': ['console', 'celery'],
            'propagate': True,
        },
        'werkzeug': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
    },
}


# CACHES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#caches
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': ''
    }
}

# TEMPLATES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#templates
TEMPLATES[0]['OPTIONS']['debug'] = DEBUG  # noqa F405

# django-debug-toolbar
# ------------------------------------------------------------------------------
# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#prerequisites
INSTALLED_APPS += ['debug_toolbar']  # noqa F405
# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#middleware
MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware']  # noqa F405
# https://django-debug-toolbar.readthedocs.io/en/latest/configuration.html#debug-toolbar-config
DEBUG_TOOLBAR_CONFIG = {
    'DISABLE_PANELS': [
        'debug_toolbar.panels.redirects.RedirectsPanel',
    ],
    'SHOW_TEMPLATE_CONTEXT': True,
}
# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#internal-ips
INTERNAL_IPS = ['127.0.0.1', '10.0.2.2']
hostname, _, ips = socket.gethostbyname_ex(socket.gethostname())
INTERNAL_IPS += [ip[:-1] + '1' for ip in ips]

# django-extensions
# ------------------------------------------------------------------------------
# https://django-extensions.readthedocs.io/en/latest/installation_instructions.html#configuration
INSTALLED_APPS += ['django_extensions']  # noqa F405


# Celery

CELERY_TASK_ALWAYS_EAGER = False

CELERY_TASK_EAGER_PROPAGATES = False
