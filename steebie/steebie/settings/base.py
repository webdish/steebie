"""
Base settings to build other settings files upon.
"""
import os
import tempfile
from decimal import Decimal

from django.conf import settings


dn = os.path.dirname
BASE_DIR = dn(dn(dn(os.path.abspath(__file__))))

LOGS_DIR = os.getenv('LOGS_DIR') or tempfile.gettempdir()


# GENERAL

DEBUG = bool(int(os.getenv('DJANGO_DEBUG', False)))

SECRET_KEY = '5BpOY0anTeei8ND3kid2rKwEO'

APPEND_SLASH = False

SITE_ID = int(os.getenv('SITE_ID'))

SKIP_APP_PROXY_VALIDATION = False

PREPEND_WWW = False


# Time & language

TIME_ZONE = 'US/Eastern'

LANGUAGE_CODE = 'en-us'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# DATABASES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.getenv('POSTGRES_DB'),
        'USER': os.getenv('POSTGRES_USER'),
        'PASSWORD': os.getenv('POSTGRES_PASSWORD'),
        'HOST': os.getenv('POSTGRES_HOST'),
        'PORT': os.getenv('POSTGRES_PORT'),
    }
}

# URLS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#root-urlconf
ROOT_URLCONF = 'steebie.urls'
# https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = 'steebie.wsgi.application'

# APPS

DJANGO_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # 'django.contrib.humanize', # Handy template tags
    'django.contrib.admin',
]
THIRD_PARTY_APPS = [
    'rest_framework',
    'bootstrap4',
    'compressor',
]
LOCAL_APPS = [
    'users.apps.UsersAppConfig',
    'campaigns.apps.CampaignsAppConfig',
    'orders.apps.OrderConfig',
    'payments.apps.PaymentsConfig',
    'customers.apps.CustomersConfig',
    'tangocard.apps.TangocardConfig',
    'base',
]

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

# AUTHENTICATION
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#authentication-backends
AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
]
# https://docs.djangoproject.com/en/dev/ref/settings/#auth-user-model
AUTH_USER_MODEL = 'users.User'
# https://docs.djangoproject.com/en/dev/ref/settings/#login-redirect-url
LOGIN_REDIRECT_URL = 'app'
# https://docs.djangoproject.com/en/dev/ref/settings/#login-url
LOGIN_URL = 'account_login'

# PASSWORDS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#password-hashers
PASSWORD_HASHERS = [
    # https://docs.djangoproject.com/en/dev/topics/auth/passwords/#using-argon2-with-django
    'django.contrib.auth.hashers.Argon2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
]
# https://docs.djangoproject.com/en/dev/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# MIDDLEWARE
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#middleware
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'base.middleware.ExceptionMiddleware',
]

# STATIC

STATIC_ROOT = os.getenv('STATIC_DIR') or os.path.join(BASE_DIR, 'staticfiles')

STATIC_URL = '/static/'

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static'),
    os.path.join(BASE_DIR, 'seller/static'),
]

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
]

# MEDIA

MEDIA_ROOT = os.getenv('MEDIA_DIR') or os.path.join(BASE_DIR, 'media')

MEDIA_URL = '/media/'


# TEMPLATES

TEMPLATES_DIR = os.path.join(BASE_DIR, 'templates')

TEMPLATES = [
    # Jinja2 templates to use with Liquid
    {
        'BACKEND': 'django_jinja.backend.Jinja2',
        'DIRS': [
            TEMPLATES_DIR,
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'cart.context_processors.shop_globals',
            ],
        },
    },
]

# FIXTURES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#fixture-dirs
FIXTURES_DIR = os.getenv('FIXTURES_DIR')
FIXTURE_DIRS = (
    FIXTURES_DIR,
)


# EMAIL
# ------------------------------------------------------------------------------

EMAIL_BACKEND = os.getenv('DJANGO_EMAIL_BACKEND', 'django.core.mail.backends.smtp.EmailBackend')

EMAIL_HOST = os.getenv('EMAIL_HOST', 'mailhog')

EMAIL_PORT = int(os.getenv('EMAIL_PORT', 1025))

EMAIL_HOST_USER = os.getenv('EMAIL_HOST_USER', '')

EMAIL_HOST_PASSWORD = os.getenv('EMAIL_HOST_PASSWORD', '')

EMAIL_USE_TLS = bool(int(os.getenv('EMAIL_USE_TLS', False)))

EMAIL_USE_SSL = bool(int(os.getenv('EMAIL_USE_SSL', False)))

DEFAULT_FROM_EMAIL = os.getenv('DJANGO_DEFAULT_FROM_EMAIL', 'Steebie Support <support@domain.com>')

SERVER_EMAIL = os.getenv('DJANGO_SERVER_EMAIL', DEFAULT_FROM_EMAIL)

EMAIL_SUBJECT_PREFIX = os.getenv('DJANGO_EMAIL_SUBJECT_PREFIX', '')


# ADMIN
# ------------------------------------------------------------------------------
# Django Admin URL.
ADMIN_URL = 'admin/'
# https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = [
    ('Ivan Shumilov', 'ivan.shumilov@gmail.com'),
]

# Celery

INSTALLED_APPS += ['taskapp.celery.CeleryAppConfig']

if USE_TZ:
    CELERY_TIMEZONE = TIME_ZONE

CELERY_BROKER_URL = os.getenv('CELERY_BROKER_URL')

CELERY_RESULT_BACKEND = CELERY_BROKER_URL

CELERY_ACCEPT_CONTENT = ['json']

CELERY_TASK_SERIALIZER = 'json'

CELERY_RESULT_SERIALIZER = 'json'

ANONYMOUS_USER_NAME = None


# Shopify

SHOPIFY_APP_API_KEY = os.getenv('SHOPIFY_APP_API_KEY')
SHOPIFY_APP_API_SECRET = os.getenv('SHOPIFY_APP_API_SECRET')
SHOPIFY_APP_API_PASSWORD = os.getenv('SHOPIFY_APP_API_PASSWORD')
SHOPIFY_STORE_URL = os.getenv('SHOPIFY_STORE_URL')
SHOPIFY_STORE_URL_PUBLIC = os.getenv('SHOPIFY_STORE_URL_PUBLIC', SHOPIFY_STORE_URL)
SHOPIFY_WEBHOOK_SECRET = os.getenv('SHOPIFY_WEBHOOK_SECRET')
SHOPIFY_PROXY_APP_API_KEY = os.getenv('SHOPIFY_PROXY_APP_API_KEY')
SHOPIFY_PROXY_APP_API_SECRET = os.getenv('SHOPIFY_PROXY_APP_API_SECRET')

SHOPIFY_LIQUID_SHARED_SECRET = os.getenv('SHOPIFY_LIQUID_SHARED_SECRET')
SHOPIFY_PROXY_PREFIX = '/apps'

SHOPIFY_GENERATED_URL = f"https://{SHOPIFY_APP_API_KEY}:{SHOPIFY_APP_API_PASSWORD}@{SHOPIFY_STORE_URL}/admin"


# Finance

STEEBIE_FEE = Decimal('2.5')

HOLD_PERIOD_DAYS = int(os.getenv('HOLD_PERIOD_DAYS', 30))

AUTO_APPROVE_PERIOD_DAYS = 5

MIN_REDEEM_STRIPE = Decimal('1.0')

MIN_REDEEM_TANGOCARD = Decimal('1.0')


# REST Framework

REST_FRAMEWORK = {
    'DEFAULT_VERSIONING_CLASS': 'rest_framework.versioning.NamespaceVersioning',
}

if DEBUG:
    REST_FRAMEWORK['DEFAULT_RENDERER_CLASSES'] = (
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
    )
else:
    REST_FRAMEWORK['DEFAULT_METADATA_CLASS'] = None
    REST_FRAMEWORK['DEFAULT_RENDERER_CLASSES'] = (
        'rest_framework.renderers.JSONRenderer',
    )


# TangoCard

TANGOCARD_CUSTOMER_ID = os.getenv('TANGOCARD_CUSTOMER_ID')
TANGOCARD_API_KEY = os.getenv('TANGOCARD_API_KEY')
TANGOCARD_API_URL = os.getenv('TANGOCARD_API_URL')
TANGOCARD_ACCOUNT_ID = os.getenv('TANGOCARD_ACCOUNT_ID')
TANGOCARD_EMAIL_TEMPLATE_ID = os.getenv('TANGOCARD_EMAIL_TEMPLATE_ID')


# Google Analytics

GA_SHOPIFY_VIEW_ID = os.getenv('GA_SHOPIFY_VIEW_ID')
