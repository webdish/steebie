from django.conf import settings
from django.conf.urls import url
from django.contrib.auth.decorators import user_passes_test, login_required
from django.urls import include, path, re_path
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView
from django.views import defaults as default_views

from campaigns.urls import (
    api_products_patterns,
    api_orders_patterns,
)
from base.views import RobotsView
from customers.urls import webhooks_customers

# API

api_patterns = [
    re_path('public/?', include(public_api_patterns)),
    re_path('products[/]?', include(api_products_patterns)),
    re_path('orders[/]?', include(api_orders_patterns)),
]

urlpatterns = [
    path('', TemplateView.as_view(template_name='index.html'), name='home'),
    path('robots.txt', RobotsView.as_view()),
    path(
        'app',
        login_required()(
            user_passes_test(lambda u: u.is_merchant)(TemplateView.as_view(template_name='merchant/home.html'))
        ),
        name='app'
    ),
    path(settings.ADMIN_URL, admin.site.urls),
    path('account/', include(('users.urls', 'users'), namespace='users')),
    # API
    path('api/v1/', include((api_patterns, 'api'), namespace='v1')),
]

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path(
            '400/',
            default_views.bad_request,
            kwargs={'exception': Exception('Bad Request!')},
        ),
        path(
            '403/',
            default_views.permission_denied,
            kwargs={'exception': Exception('Permission Denied')},
        ),
        path(
            '404/',
            default_views.page_not_found,
            kwargs={'exception': Exception('Page not Found')},
        ),
        path('500/', default_views.server_error),
    ]

    urlpatterns += static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
    )

    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [path('__debug__/', include(debug_toolbar.urls))] + urlpatterns
