from django.apps import AppConfig


class UsersAppConfig(AppConfig):

    name = 'users'
    verbose_name = 'Users'

    def ready(self):
        from .signals import set_merchant  # noqa 401
