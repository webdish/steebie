from django.urls import path

from .views import UserDetailView

urlpatterns = [
    path('', view=UserDetailView.as_view(), name='account'),
]
