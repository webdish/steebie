from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DetailView


class UserDetailView(LoginRequiredMixin, DetailView):

    def get_object(self, queryset=None):
        return self.request.user

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        email = ctx['object'].emailaddress_set.filter(primary=True).first()
        if email:
            ctx['email'] = email.email

        return ctx
