from django.contrib.auth.models import AbstractUser, Group
from django.urls import reverse


class User(AbstractUser):

    def get_absolute_url(self):
        return reverse('users:detail', kwargs={'username': self.username})

    def set_merchant(self):
        self.groups.add(Group.objects.get(name='merchants'))
        self.save()

    @property
    def is_merchant(self):
        return self.groups.filter(name='merchants').count() == 1
