from decimal import Decimal
import datetime
import time

from django.db import models

from shopify import Metafield

from base.exceptions import ShopifyApiError
from utils.shopify import set_shopify_site


class BaseShopify(models.Model):
    """Base model for Shopify data"""

    # List of Shopify attributes
    shopify_fields = []

    # Map: Shopify attributes names -> model attributes names (if differ)
    # E.g. you have model field shopify_id that represents Shopify id attribute, add it like that:
    # {'id': 'shopify_id'}
    shopify_fields_map = {}

    # Shopify resource class
    # E.g. shopify.Product
    shopify_resource_cls = None

    # Nested models names
    shopify_nested = []
    # Field holds parent object
    shopify_parent = None
    # Field holds Shopify parent object
    shopify_parent_id_name = None

    metafields_namespace = 'steebie'

    @classmethod
    def get_shopify_fields(cls):
        """Returns Shopify -> Model fields names mapping (key is Shopify name, value is model name)

        Returns:
            dict[str, str]
        """
        names = {k: k for k in cls.shopify_fields}
        names.update(**cls.shopify_fields_map)
        return names

    def get_shopify_id(self):
        """Returns Shopify ID

        Returns:
            int
        """
        return getattr(self, self.get_shopify_fields()['id'])

    def _get_parent(self):
        """Returns parent object

        Returns:
            BaseShopify
        """
        if self.shopify_parent:
            return getattr(self, self.shopify_parent)

    def _iter_shopify_fields(self):
        for shopify_name, model_name in self.get_shopify_fields().items():
            yield shopify_name, getattr(self, model_name)

    def as_dict(self):
        """Returns Shopify resource attributes as dict

        Returns:
            dict
        """
        data = {}
        for name, value in self._iter_shopify_fields():
            if isinstance(value, Decimal):
                # Decimal is not JSON serializable, we need to convert it to string
                value = str(value)
            if isinstance(value, (datetime.date, datetime.datetime)):
                # Date/datetime is not JSON serializable, we need to convert it to string
                value = str(value)
            data[name] = value
        # Set parent ID
        if self.shopify_parent:
            data[self.shopify_parent_id_name] = self._get_parent().get_shopify_id()
        return data

    def as_shopify_resource(self):
        """Returns object as Shopify resource

        Returns:
            shopify.base.ShopifyResource
        """
        if self.shopify_resource_cls:
            return self.shopify_resource_cls(attributes=self.as_dict())

    @set_shopify_site
    def get_resource(self):
        """Hits Shopify API and gets the resource

        Returns:
            shopify.base.ShopifyResource
        """
        if self.get_shopify_id():
            return self.shopify_resource_cls.find(id_=self.get_shopify_id())

    def get_nested(self, name):
        # Get realted name
        related_name = self.shopify_fields_map.get(name, name)
        # Get nested queryset
        return getattr(self, related_name).all()

    def get_metafields(self):
        """
        Return Shopify resource metafields data as a list of dicts. Should be overridden in the child classes if needed

        Returns:
            list[dict]
        """
        return []

    def _set_metafields(self, resource):
        for item in self.get_metafields():
            item.update({'namespace': self.metafields_namespace})
            resource.add_metafield(Metafield(item))

    @set_shopify_site
    def sync(self, nested=True):
        """Syncs with Shopify and updates the instance

        Args:
            nested (bool): Sync nested objects (if exist)
        Returns:
            None
        Raise:
            base.exceptions.ShopifyApiError
        """
        resource = self.as_shopify_resource()

        # Don't update nested items from resource if it's a first time sync
        nested_resource = False
        if self.get_shopify_id():
            nested_resource = True

        if resource.save():
            self.update_from_resource(resource, nested=nested_resource)

            # Set metafields
            time.sleep(1)
            self._set_metafields(resource)

            if nested:
                for name in self.shopify_nested:
                    for obj in self.get_nested(name):
                        time.sleep(1)
                        obj.sync()

            return resource
        else:
            raise ShopifyApiError(
                f'Error saving {self.__class__.__name__} {self.pk}. {". ".join(resource.errors.full_messages())}'
            )

    @classmethod
    def from_json(cls, data, save=True, nested=True):
        """Creates instance from data came from Shopify API

        Args:
            data (dict):
            save (bool): save created instance; optional
            nested (bool): create neted objects too
        Returns:
            BaseShopify
        """
        instance = cls()
        instance.update(data, save=save, nested=nested)
        return instance

    def update_from_resource(self, resource, fields=None, save=True, nested=True):
        """Updates object from from Shopify resource

        Args:
            resource (shopify.base.ShopifyResource):
            fields (list): fields names which needs to be updated; optional
            save (bool): save updated instance; optional
            nested:
        Returns:
            None
        """
        self.update(resource.to_dict(), fields=fields, save=save, nested=nested)

    def update(self, data, fields=None, save=True, nested=True):
        """Updates object from data came from Shopify API

        Args:
            data (dict):
            fields (list): fields names which needs to be updated; optional
            save (bool): save updated instance; optional
            nested (bool): update nested objects too; these objects will be saved anyway
                ignoring the `save` & `fields` options; optional
        Returns:
            None
        """
        if not fields:
            # Get Shopify fields names list
            fields = self.get_shopify_fields().keys()

        for k in fields:
            if k in data:
                field = self.shopify_fields_map.get(k, k)
                setattr(self, field, data[k])

        if save:
            self.save()

        if nested:
            self.update_nested(data)

    def update_nested(self, data):
        """Updates/creates nested models from data came from Shopify API

        Args:
            data (dict):
        Returns:
            None
        """
        # Iterate nested types
        for nested in self.shopify_nested:
            # Get Shopify nested data
            _data = data.get(nested, [])
            related_name = self.shopify_fields_map.get(nested, nested)
            # Get nested queryset
            qs = getattr(self, related_name).all()
            model = qs.model
            model_shopify_id = model.get_shopify_fields()['id']

            # Iterate over Shopify nested objects
            for item in _data:
                if qs.filter(**{model_shopify_id: item['id']}).exists():
                    # If we found existing object update it
                    obj = qs.get(**{model_shopify_id: item['id']})
                    obj.update(item)
                else:
                    # Otherwise create new nested object
                    obj = model.from_json(item, save=False)
                    # Set parent object
                    setattr(obj, obj.shopify_parent, self)
                    obj.save()

    class Meta:
        abstract = True
