from django.contrib import admin


class NoAddAdminMixin:
    # noinspection PyMethodMayBeStatic
    def has_add_permission(self, *args, **kwargs):
        return False


class NoChangeAdminMixin:
    # noinspection PyMethodMayBeStatic
    def has_change_permission(self, request, obj=None):
        return False


class NoDeleteAdmin(admin.ModelAdmin):
    def get_actions(self, request):
        actions = super(NoDeleteAdmin, self).get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def has_delete_permission(self, *args, **kwargs):
        return False


class ReadOnlyAdmin(NoAddAdminMixin, NoChangeAdminMixin, NoDeleteAdmin):

    def get_readonly_fields(self, request, obj=None):
        fields = list(self.readonly_fields)
        fields.extend([field.name for field in obj._meta.fields])
        fields.extend([field.name for field in obj._meta.many_to_many])

        return fields
