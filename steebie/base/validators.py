import logging
import string
from decimal import Decimal
import re

from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

import requests
import phonenumbers
from twilio.base.exceptions import TwilioRestException
from twilio.rest import Client

from base.twilio import TwilioHttpClientTimeout

log = logging.getLogger(__name__)


def keywords(value):
    if len(list(filter(None, map(str.strip, value.split())))) > 5:
        raise ValidationError(_('Maximum number of keywords must not exceed 5.'))


def joined_list_length(value, max_length=255):
    if len(' '.join(value)) > max_length:
        raise ValidationError(_(f'Total length of all items including delimiters should not exceed {max_length}'))


def percent(value, value_min=Decimal('0.01'), value_max=Decimal('99.9')):
    if value < value_min or value > value_max:
        raise ValidationError(_(f'The value must be between {value_min} and {value_max}'))


def reward_limits(value):
    return percent(value, value_min=Decimal('1.0'), value_max=Decimal('99.0'))


def promo_limits(value):
    return percent(value, value_min=Decimal('0.01'), value_max=Decimal('100.0'))


def gt_zero(value):
    if value <= 0:
        raise ValidationError(_(f'The value should be greater than zero'))


def _alphanum_string_inner(value, allowed):
    for s in value:
        if s not in allowed:
            raise ValidationError(_(f"'{s}' is not allowed here."))


def alphanum_string(value):
    allowed = string.digits + string.ascii_letters + '_-'
    return _alphanum_string_inner(value, allowed)


def alphanum_space_string(value):
    allowed = string.digits + string.ascii_letters + '_- '
    return _alphanum_string_inner(value, allowed)


def us_phone_number(value):
    try:
        number = phonenumbers.parse(value, region='US')
        if phonenumbers.is_valid_number(number):
            return phonenumbers.format_number(number, phonenumbers.PhoneNumberFormat.E164)
    except phonenumbers.NumberParseException:
        pass

    raise ValidationError(_('Phone number is invalid.'))


def us_phone_number_twilio(value):
    # If we are in production
    if not settings.TWILIO_TEST_MODE:
        http_client = TwilioHttpClientTimeout(pool_connections=True, timeout=3)
        client = Client(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTHTOKEN, http_client=http_client)
        try:
            number = client.lookups.phone_numbers(value).fetch(country_code='US')
            return number.phone_number
        except TwilioRestException as e:
            if e.status == 404:
                raise ValidationError(_('Phone number is invalid.'))
            log.error(e, exc_info=True)
        except requests.exceptions.RequestException as e:
            log.warning(e)

    # If we are in test mode / fallback
    return us_phone_number(value)


def amazon_order_num(value):
    if not re.match(r'\d{3}-\d{7}-\d{7}', value):
        raise ValidationError('Invalid Amazon order number.')
