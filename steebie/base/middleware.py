from django.http import Http404
from django.utils.deprecation import MiddlewareMixin
from sentry_sdk import capture_exception


class ExceptionMiddleware(MiddlewareMixin):
    def process_exception(self, request, exception):
        if not isinstance(exception, (Http404,)):
            try:
                raise Exception(f'Error at {request.path}') from exception
            except Exception as e:
                capture_exception(error=e)
