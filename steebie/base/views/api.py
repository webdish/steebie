from django.core.exceptions import ObjectDoesNotExist

from rest_framework.authentication import SessionAuthentication
from rest_framework.exceptions import NotFound
from rest_framework.parsers import JSONParser
from rest_framework.permissions import DjangoModelPermissions, IsAuthenticated
from rest_framework.views import APIView

from base.auth import IsMerchant


class BaseAPIResource(APIView):

    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated, DjangoModelPermissions, IsMerchant)
    parser_classes = (JSONParser,)
    model = None

    def get_queryset(self, **kwargs):
        return self.model.objects.filter(**kwargs)


class BaseAPIObjectResource(BaseAPIResource):

    def get_object(self, qs=None):
        if not qs:
            qs = self.get_queryset()
        try:
            return qs.get(pk=self.kwargs['pk'])
        except ObjectDoesNotExist:
            raise NotFound
