from django.contrib.sites.shortcuts import get_current_site
from django.views.generic import TemplateView
from django.conf import settings


class RobotsView(TemplateView):

    template_name = 'robots.txt'
    content_type = 'text/plain'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        site = get_current_site(self.request)
        if site.domain != settings.PRODUCTION_DOMAIN:
            ctx['disallow'] = True

        return ctx
