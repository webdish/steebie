# Prerequisites

## Dependencies

You'll need Python 3.6, Git, [Docker](https://docs.docker.com/install/) v17.12, [Docker Compose](https://docs.docker.com/compose/install/) v1.17 or newer.

## Python Requirements

requirements.txt

## Docker

Docker Compose is used to run the project both in development & production.