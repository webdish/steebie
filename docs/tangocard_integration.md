# TangoCard Integration

## API Access

You need to set values to these variables:

* `TANGOCARD_CUSTOMER_ID`
* `TANGOCARD_API_KEY`
* `TANGOCARD_ACCOUNT_ID`

Go to [https://integration-www.tangocard.com/raas_api_console/v2/](https://integration-www.tangocard.com/raas_api_console/v2/) to obtain test tokens and create account.

Use 'Platform Name' as `TANGOCARD_CUSTOMER_ID` and 'Platform Key'  as `TANGOCARD_API_KEY`.