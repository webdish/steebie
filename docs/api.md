# Steebie Backend API

## Specification

Docs on SwaggerHub: [swagger.io/specification](https://swagger.io/specification/)

Bad request error note: SwaggerHub displays it incorrectly, use YAML spec instead.