# Shopify Integration

## Stores

Different stores are used for different environments:

* `steebie.myshopify.com` is for production
* `steebiestaging.myshopify.com` is for staging
* `steebieqa.myshopify.com` is for QA
* `steebiedev.myshopify.com` is for dev
* your personal `xxxx.myshopify.com` is for local development.


## API Access

You need to set values to these variables:

* `SHOPIFY_APP_API_KEY`
* `SHOPIFY_APP_API_SECRET`
* `SHOPIFY_APP_API_PASSWORD`


## Webhooks

Shopify Admin URL: `https://<store-name>.myshopify.com/admin/settings/notifications`.

Go to Shopify Admin and get webhook secret from the bottom of Webhooks section. It needs to be assigned to `SHOPIFY_WEBHOOK_SECRET` in `django.env` file.

### Create/Update Webhooks

At the bottom of the page you can create or update existing webhooks.

Use appropriate Steebie domain or your personal ngrok domain as webhook domain. Set Shopify API version to `2019-04`.

List of existing webhooks endpoints:

Webhook | Endpoint
--- | ---
Collection Update | `/webhooks/collections/updated`
Collection Deletion | `/webhooks/collections/deleted`
Customer Creation | `/webhooks/customers/created`
Customer Update | `/webhooks/customers/updated`
Customer Disable | `/webhooks/customers/disabled`
Customer Enable | `/webhooks/customers/enabled`
Customer Deletion | `/webhooks/customers/deleted`
Product Update | `/webhooks/products/updated`
Product Deletion | `/webhooks/products/deleted`


## Proxy App

You need to set values to these variables:

* `SHOPIFY_PROXY_APP_API_KEY`
* `SHOPIFY_PROXY_APP_API_SECRET`

Go to [Shopify Partners](https://partners.shopify.com/) / Apps / Steebie Merchant / Extensions / Online store / App proxy / Manage app proxy and set up your actual app proxy URL & subpath. If you change subpath prefix you must also update `SHOPIFY_PROXY_PREFIX` in `django.env` file with actual value.

See [app proxies guide](https://help.shopify.com/en/api/guides/application-proxies) for details.