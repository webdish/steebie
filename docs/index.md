# Steebie Backend Docs Index

## Contents

[Shopify Integration](shopify_integration.md)

[TangoCard Integration](tangocard_integration.md)

[How to Run in Development](how_to_run_in_development.md)

[API](api.md)


## How to Use It

1. Read this guide carefully
2. Follow the setup steps
3. If something goes wrong make sure you did all as described
4. If something goes wrong and you're pretty sure you did all as described, save all debug info (error message, logs, etc) and report error
5. If you find some mistakes/gaps/understatements in this guide report them please

## Notes

Most shell commands in this manual are supposed to be executed from the project root dir (where `docs` dir is located).

Dollar sign `$` before command means it should be executed from ordinary user, sharp sign `#` -- from root (or via `sudo`).