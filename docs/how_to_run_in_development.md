# How to Run in Development

## Config

`compose/local` -- dockerfiles, environment files, starting scripts and other staff needed by containers.

`compose_local.yml` -- compose configuration.

`compose_prod.yml` -- production compose configuration for local testing.

## Build & Run

**Create environment files:** refer environment files documentation for details.

**Build:**

```bash
sudo docker-compose -f compose_local.yml -p steebie build
```

**Create fixtures:** refer fixtures documentation for details.

**Run:**

```bash
sudo docker-compose -f compose_local.yml -p steebie up
```

Django development server will be available on: [http://localhost:8000](http://localhost:8000).

Command above runs only specified services (`django`) & its dependencies (`postgres`, `mailhog`).

## Rebuild

If you modify requirements you need to rebuild containers for the changes to take effect.

Django server & Celery worker will reboot automatically when code changes.

Email section of your `production/django/django.env` should looks like that:

```
EMAIL_HOST=mailhog
EMAIL_PORT=1025
EMAIL_HOST_USER=
EMAIL_HOST_PASSWORD=
EMAIL_USE_TLS=0
EMAIL_USE_SSL=0
```
